"use strict";

var path         = require('path');
var isrequirable = require('isrequirable');

module.exports = function (type, name, options, alias) {
	alias = alias || name;

	if (type === 'config') {
		if (this.config.hasOwnProperty(alias)) {
			console.warn('Config', alias, 'already loaded.');
			return;
		}

		var configPath = path.join(this.CONFIG_DIR, name);

		if (isrequirable(configPath)) {
			this.config[alias] = this._require().config(name);

			console.log('Config', alias, 'loaded.');
		} else {
			console.error('Config', alias, 'not found.');
		}
	} else if (type === 'lib') {
		if (this.lib.hasOwnProperty(alias)) {
			console.warn('Library', alias, 'already loaded.');
			return;
		}

		var LIB_MODULE_PREFIX = 'mythos-lib-';
		var libPath = path.join(this.LIB_DIR, name);

		if (isrequirable(LIB_MODULE_PREFIX + name)) {
			var LibClass = require(LIB_MODULE_PREFIX + name);
			this.lib[alias] = new LibClass(this, options);

			console.log('Library', alias, 'loaded.');
		} else if (isrequirable(libPath)) {
			var LibClass = this._require().lib(name);
			this.lib[alias] = new LibClass(this, options);

			console.log('Library', alias, 'loaded.');
		} else {
			console.error('Library', alias, 'not found.');
		}
	}
};