"use strict";

var path = require('path');

var Mythos = function (appDir) {
	this.APP_DIR        = appDir;
	this.CONFIG_DIR     = path.join(this.APP_DIR, 'config');
	this.CONTROLLER_DIR = path.join(this.APP_DIR, 'controllers');
	this.LIB_DIR        = path.join(this.APP_DIR, 'lib');
	this.MODEL_DIR      = path.join(this.APP_DIR, 'models');
	this.PUBLIC_DIR     = path.join(this.APP_DIR, 'public');
	this.VIEW_DIR       = path.join(this.APP_DIR, 'views');

	this.config  = {};
	this.lib     = {};
	this.model   = {};
};

Mythos.init = function (app) {
	var APP_DIR   = path.dirname(module.parent.filename);
	var HOOKS_DIR = path.join(APP_DIR, 'hooks');

	require(HOOKS_DIR + '/pre-mythos')(app);

	app.use(function (req, res, next) {
		var mythos = new Mythos(APP_DIR);
		mythos.autoload();
		res.locals.mythos = mythos;
		next();
	});

	require(HOOKS_DIR + '/pre-routes')(app);
	require(HOOKS_DIR + '/routes')(app);
	require(HOOKS_DIR + '/post-routes')(app);
};

Mythos.prototype._require = require('./_require');
Mythos.prototype.autoload = require('./autoload');
Mythos.prototype.load     = require('./load');

module.exports = Mythos;