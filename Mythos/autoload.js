"use strict";

module.exports = function () {
	var autoloadConfig = this._require().config('autoload');

	for (var i in autoloadConfig.config) {
		var name = i;
		this.load('config', name);
	}

	for (var i in autoloadConfig.lib) {
		var name = autoloadConfig.lib[i];
		var params = null;
		var options = null;
		var alias = null;

		if (typeof name !== 'string') {
			params = name;
			name   = i;

			if (params.hasOwnProperty('options') && params.options) {
				options = params.options;
			}

			if (params.hasOwnProperty('config') && params.config) {
				if (typeof params.config === 'string') {
					options = this._require().config(params.config);
				} else {
					options = this._require().config(name);
				}
			}

			if (params.hasOwnProperty('alias') && params.alias) {
				alias = params.alias;
			}
		} else {
			params = null;
			alias  = name;
		}

		this.load('lib', name, options, alias);
	}
};