"use strict";

var path         = require('path');
var isrequirable = require('isrequirable');

module.exports = function () {
	var self = this;

	var requireFunc = function (dir, fileName) {
		var filePath = path.join(dir, fileName);

		if (isrequirable(filePath)) {
			return require(path.join(dir, fileName));
		} else {
			var err = new Error("File not found: " + filePath);
			console.error(err);
			throw err;
		}
	};

	return {
		"config": function (fileName) {
			return requireFunc(self.CONFIG_DIR, fileName);
		},
		"lib": function (fileName) {
			return requireFunc(self.LIB_DIR, fileName);
		},
		"model": function (fileName) {
			return requireFunc(self.MODEL_DIR, fileName);
		}
	};
};